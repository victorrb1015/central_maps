<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'victor',
            'email' => 'victor@gmail.com',
            'password' => bcrypt('123456789'),
        ]);
        DB::table('users')->insert([
            'name' => 'john',
            'email' => 'john@gmail.com',
            'password' => bcrypt('Administrador1'),
        ]);
        DB::table('gerentes')->insert([
            'nombre' => 'Por definir',
            'email' => 'default@defautl.com',
            'telefono' => '55555555',
            'empresa' => 'Central',
        ]);
        DB::table('permisos')->insert([
            'nombre' => 'Recursos Humanos'
        ]);
        DB::table('roles')->insert([
            'id_permiso' => 1,
            'id_user' => 1
        ]);
        DB::table('roles')->insert([
            'id_permiso' => 1,
            'id_user' => 2
        ]);
        DB::table('marcas')->insert([
            'marca' => 'Default'
        ]);
        DB::table('gerentes')->insert([
            'nombre' => 'Sin gerente',
            'email' => 'default@default.com',
            'telefono' => '5555555555',
            'empresa' => 'Central',
            'Estado' => 'Activo'
        ]);

    }
}
