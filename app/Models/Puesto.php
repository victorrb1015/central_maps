<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Puesto
 * @package App\Models
 * @version August 22, 2019, 3:01 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection
 * @property \Illuminate\Database\Eloquent\Collection vacantes
 * @property string puesto
 * @property string area
 * @property string req
 * @property string des
 */
class Puesto extends Model
{

    public $table = 'puestos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'puesto',
        'area',
        'req',
        'des'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'puesto' => 'string',
        'area' => 'string',
        'req' => 'string',
        'des' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'puesto' => 'required',
        'area' => 'required',
        'req' => 'required',
        'des' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function vacantes()
    {
        return $this->hasMany(\App\Models\Vacante::class);
    }
}
