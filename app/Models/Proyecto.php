<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Proyecto
 * @package App\Models
 * @version September 2, 2019, 4:12 pm UTC
 *
 * @property \App\Models\Gerente idGerente
 * @property \App\Models\Marca idMarca
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property \Illuminate\Database\Eloquent\Collection vacantes
 * @property integer id_gerente
 * @property integer id_marca
 * @property string nombre
 * @property string direccion
 * @property string pais
 * @property string edo_prov
 * @property string mun_cd
 * @property string colonia
 * @property string calles
 * @property string numeroExt
 * @property string cp
 * @property string latitud
 * @property string longitud
 * @property boolean status
 * @property integer num_vac
 * @property string foto
 * @property boolean escuela
 * @property integer no_est
 */
class Proyecto extends Model
{

    public $table = 'proyectos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'id_gerente',
        'id_marca',
        'nombre',
        'direccion',
        'pais',
        'edo_prov',
        'mun_cd',
        'colonia',
        'calles',
        'numeroExt',
        'cp',
        'latitud',
        'longitud',
        'status',
        'num_vac',
        'foto',
        'escuela',
        'no_est'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_gerente' => 'integer',
        'id_marca' => 'integer',
        'nombre' => 'string',
        'direccion' => 'string',
        'pais' => 'string',
        'edo_prov' => 'string',
        'mun_cd' => 'string',
        'colonia' => 'string',
        'calles' => 'string',
        'numeroExt' => 'string',
        'cp' => 'string',
        'latitud' => 'string',
        'longitud' => 'string',
        'status' => 'boolean',
        'num_vac' => 'integer',
        'foto' => 'string',
        'escuela' => 'boolean',
        'no_est' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_gerente' => 'required',
        'id_marca' => 'required',
        'nombre' => 'required',
        'direccion' => 'required',
        'pais' => 'required',
        'edo_prov' => 'required',
        'mun_cd' => 'required',
        'colonia' => 'required',
        'calles' => 'required',
        'numeroExt' => 'required',
        'cp' => 'required',
        'latitud' => 'required',
        'longitud' => 'required',
        'status' => 'required',
        'num_vac' => 'required',
        'foto' => 'required',
        'escuela' => 'required',
        'no_est' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idGerente()
    {
        return $this->belongsTo(\App\Models\Gerente::class, 'id_gerente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idMarca()
    {
        return $this->belongsTo(\App\Models\Marca::class, 'id_marca');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function vacantes()
    {
        return $this->hasMany(\App\Models\Vacante::class);
    }
}
