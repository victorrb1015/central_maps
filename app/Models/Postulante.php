<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Postulante
 * @package App\Models
 * @version August 22, 2019, 3:03 pm UTC
 *
 * @property \App\Models\Vacante idVacante
 * @property \Illuminate\Database\Eloquent\Collection
 * @property integer id_vacante
 * @property string nombre
 * @property string correo
 * @property string tel
 * @property string direccion
 * @property string pais
 * @property string edo_prov
 * @property string mun_cd
 * @property string colonia
 * @property string calles
 * @property string numeroExt
 * @property string cp
 * @property string estado
 */
class Postulante extends Model
{

    public $table = 'postulantes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'id_vacante',
        'nombre',
        'correo',
        'tel',
        'direccion',
        'pais',
        'edo_prov',
        'mun_cd',
        'colonia',
        'calles',
        'numeroExt',
        'cp',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_vacante' => 'integer',
        'nombre' => 'string',
        'correo' => 'string',
        'tel' => 'string',
        'direccion' => 'string',
        'pais' => 'string',
        'edo_prov' => 'string',
        'mun_cd' => 'string',
        'colonia' => 'string',
        'calles' => 'string',
        'numeroExt' => 'string',
        'cp' => 'string',
        'estado' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_vacante' => 'required',
        'nombre' => 'required',
        'correo' => 'required',
        'tel' => 'required',
        'direccion' => 'required',
        'pais' => 'required',
        'edo_prov' => 'required',
        'mun_cd' => 'required',
        'colonia' => 'required',
        'calles' => 'required',
        'numeroExt' => 'required',
        'cp' => 'required',
        'estado' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idVacante()
    {
        return $this->belongsTo(\App\Models\Vacante::class, 'id_vacante');
    }
}
