<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePostulanteAPIRequest;
use App\Http\Requests\API\UpdatePostulanteAPIRequest;
use App\Models\Postulante;
use App\Repositories\PostulanteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PostulanteController
 * @package App\Http\Controllers\API
 */
class PostulanteAPIController extends AppBaseController
{
    /** @var  PostulanteRepository */
    private $postulanteRepository;

    public function __construct(PostulanteRepository $postulanteRepo)
    {
        $this->postulanteRepository = $postulanteRepo;
    }

    /**
     * Display a listing of the Postulante.
     * GET|HEAD /postulantes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $postulantes = $this->postulanteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($postulantes->toArray(), 'Postulantes retrieved successfully');
    }

    /**
     * Store a newly created Postulante in storage.
     * POST /postulantes
     *
     * @param CreatePostulanteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePostulanteAPIRequest $request)
    {
        $input = $request->all();

        $postulante = $this->postulanteRepository->create($input);

        return $this->sendResponse($postulante->toArray(), 'Postulante saved successfully');
    }

    /**
     * Display the specified Postulante.
     * GET|HEAD /postulantes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Postulante $postulante */
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            return $this->sendError('Postulante not found');
        }

        return $this->sendResponse($postulante->toArray(), 'Postulante retrieved successfully');
    }

    /**
     * Update the specified Postulante in storage.
     * PUT/PATCH /postulantes/{id}
     *
     * @param int $id
     * @param UpdatePostulanteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostulanteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Postulante $postulante */
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            return $this->sendError('Postulante not found');
        }

        $postulante = $this->postulanteRepository->update($input, $id);

        return $this->sendResponse($postulante->toArray(), 'Postulante updated successfully');
    }

    /**
     * Remove the specified Postulante from storage.
     * DELETE /postulantes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Postulante $postulante */
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            return $this->sendError('Postulante not found');
        }

        $postulante->delete();

        return $this->sendResponse($id, 'Postulante deleted successfully');
    }
}
