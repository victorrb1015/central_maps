<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGerenteAPIRequest;
use App\Http\Requests\API\UpdateGerenteAPIRequest;
use App\Models\Gerente;
use App\Repositories\GerenteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class GerenteController
 * @package App\Http\Controllers\API
 */
class GerenteAPIController extends AppBaseController
{
    /** @var  GerenteRepository */
    private $gerenteRepository;

    public function __construct(GerenteRepository $gerenteRepo)
    {
        $this->gerenteRepository = $gerenteRepo;
    }

    /**
     * Display a listing of the Gerente.
     * GET|HEAD /gerentes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $gerentes = $this->gerenteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($gerentes->toArray(), 'Gerentes retrieved successfully');
    }

    /**
     * Store a newly created Gerente in storage.
     * POST /gerentes
     *
     * @param CreateGerenteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGerenteAPIRequest $request)
    {
        $input = $request->all();

        $gerente = $this->gerenteRepository->create($input);

        return $this->sendResponse($gerente->toArray(), 'Gerente saved successfully');
    }

    /**
     * Display the specified Gerente.
     * GET|HEAD /gerentes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Gerente $gerente */
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            return $this->sendError('Gerente not found');
        }

        return $this->sendResponse($gerente->toArray(), 'Gerente retrieved successfully');
    }

    /**
     * Update the specified Gerente in storage.
     * PUT/PATCH /gerentes/{id}
     *
     * @param int $id
     * @param UpdateGerenteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGerenteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Gerente $gerente */
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            return $this->sendError('Gerente not found');
        }

        $gerente = $this->gerenteRepository->update($input, $id);

        return $this->sendResponse($gerente->toArray(), 'Gerente updated successfully');
    }

    /**
     * Remove the specified Gerente from storage.
     * DELETE /gerentes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Gerente $gerente */
        $gerente = $this->gerenteRepository->find($id);

        if (empty($gerente)) {
            return $this->sendError('Gerente not found');
        }

        $gerente->delete();

        return $this->sendResponse($id, 'Gerente deleted successfully');
    }
}
