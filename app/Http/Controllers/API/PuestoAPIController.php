<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePuestoAPIRequest;
use App\Http\Requests\API\UpdatePuestoAPIRequest;
use App\Models\Puesto;
use App\Repositories\PuestoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PuestoController
 * @package App\Http\Controllers\API
 */
class PuestoAPIController extends AppBaseController
{
    /** @var  PuestoRepository */
    private $puestoRepository;

    public function __construct(PuestoRepository $puestoRepo)
    {
        $this->puestoRepository = $puestoRepo;
    }

    /**
     * Display a listing of the Puesto.
     * GET|HEAD /puestos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $puestos = $this->puestoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($puestos->toArray(), 'Puestos retrieved successfully');
    }

    /**
     * Store a newly created Puesto in storage.
     * POST /puestos
     *
     * @param CreatePuestoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePuestoAPIRequest $request)
    {
        $input = $request->all();

        $puesto = $this->puestoRepository->create($input);

        return $this->sendResponse($puesto->toArray(), 'Puesto saved successfully');
    }

    /**
     * Display the specified Puesto.
     * GET|HEAD /puestos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Puesto $puesto */
        $puesto = $this->puestoRepository->find($id);

        if (empty($puesto)) {
            return $this->sendError('Puesto not found');
        }

        return $this->sendResponse($puesto->toArray(), 'Puesto retrieved successfully');
    }

    /**
     * Update the specified Puesto in storage.
     * PUT/PATCH /puestos/{id}
     *
     * @param int $id
     * @param UpdatePuestoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePuestoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Puesto $puesto */
        $puesto = $this->puestoRepository->find($id);

        if (empty($puesto)) {
            return $this->sendError('Puesto not found');
        }

        $puesto = $this->puestoRepository->update($input, $id);

        return $this->sendResponse($puesto->toArray(), 'Puesto updated successfully');
    }

    /**
     * Remove the specified Puesto from storage.
     * DELETE /puestos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Puesto $puesto */
        $puesto = $this->puestoRepository->find($id);

        if (empty($puesto)) {
            return $this->sendError('Puesto not found');
        }

        $puesto->delete();

        return $this->sendResponse($id, 'Puesto deleted successfully');
    }
}
