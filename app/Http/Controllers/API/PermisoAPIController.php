<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePermisoAPIRequest;
use App\Http\Requests\API\UpdatePermisoAPIRequest;
use App\Models\Permiso;
use App\Repositories\PermisoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PermisoController
 * @package App\Http\Controllers\API
 */
class PermisoAPIController extends AppBaseController
{
    /** @var  PermisoRepository */
    private $permisoRepository;

    public function __construct(PermisoRepository $permisoRepo)
    {
        $this->permisoRepository = $permisoRepo;
    }

    /**
     * Display a listing of the Permiso.
     * GET|HEAD /permisos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $permisos = $this->permisoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($permisos->toArray(), 'Permisos retrieved successfully');
    }

    /**
     * Store a newly created Permiso in storage.
     * POST /permisos
     *
     * @param CreatePermisoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePermisoAPIRequest $request)
    {
        $input = $request->all();

        $permiso = $this->permisoRepository->create($input);

        return $this->sendResponse($permiso->toArray(), 'Permiso saved successfully');
    }

    /**
     * Display the specified Permiso.
     * GET|HEAD /permisos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Permiso $permiso */
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            return $this->sendError('Permiso not found');
        }

        return $this->sendResponse($permiso->toArray(), 'Permiso retrieved successfully');
    }

    /**
     * Update the specified Permiso in storage.
     * PUT/PATCH /permisos/{id}
     *
     * @param int $id
     * @param UpdatePermisoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePermisoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Permiso $permiso */
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            return $this->sendError('Permiso not found');
        }

        $permiso = $this->permisoRepository->update($input, $id);

        return $this->sendResponse($permiso->toArray(), 'Permiso updated successfully');
    }

    /**
     * Remove the specified Permiso from storage.
     * DELETE /permisos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Permiso $permiso */
        $permiso = $this->permisoRepository->find($id);

        if (empty($permiso)) {
            return $this->sendError('Permiso not found');
        }

        $permiso->delete();

        return $this->sendResponse($id, 'Permiso deleted successfully');
    }
}
