<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateVacanteRequest;
use App\Http\Requests\UpdateVacanteRequest;
use App\Models\Postulante;
use App\Models\Proyecto;
use App\Models\Puesto;
use App\Models\Vacante;
use App\Repositories\VacanteRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class VacanteController extends AppBaseController
{
    /** @var  VacanteRepository */
    private $vacanteRepository;

    public function __construct(VacanteRepository $vacanteRepo)
    {
        $this->vacanteRepository = $vacanteRepo;
    }

    /**
     * Display a listing of the Vacante.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $vacantes = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->select('vacantes.*', 'proyectos.nombre as estacionamiento', 'puestos.puesto', 'puestos.area')
            ->get();
        return view('vacantes.index')
            ->with('vacantes', $vacantes);
    }

    /**
     * Show the form for creating a new Vacante.
     *
     * @return Response
     */
    public function create()
    {
        $proyectos = Proyecto::all();
        $puestos = Puesto::all();
        return view('vacantes.create')
            ->with('proyectos', $proyectos)
            ->with('puestos', $puestos);
    }

    /**
     * Store a newly created Vacante in storage.
     *
     * @param CreateVacanteRequest $request
     *
     * @return Response
     */
    public function store()
    {

        $input = request()->validate([
            'user' => ['required'],
            'estacionamiento' => ['required'],
            'puesto' => ['required'],
            "inicio" => ['required'],
            "fin" => ['required'],
            "fecha" => ['required'],
            "fecha_submit" => ['required'],
            "salario" => ['required'],
            "tel" => ['required'],
            "email" => ['required'],
            "estado" => ['required']
        ]);


        $vacante = new Vacante();
        $vacante->fill([
            'id_proyecto' => $input['estacionamiento'],
            'id_puesto' => $input['puesto'],
            'id_user' => $input['user'],
            'horario' => $input['inicio'] . " - " . $input['fin'],
            'salario' => $input['salario'],
            'fecha' => $input['fecha'],
            'tel' => $input['tel'],
            'email' => $input['email'],
            'estado' => $input['estado']
        ]);
        $vacante->save();

        Flash::success('Vacante agregada correctamente!.');

        return redirect(route('vacantes.index'));
    }

    /**
     * Display the specified Vacante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vacante = $this->vacanteRepository->find($id);

        if (empty($vacante)) {
            Flash::error('Vacante not found');

            return redirect(route('vacantes.index'));
        }

        return view('vacantes.show')->with('vacante', $vacante);
    }

    /**
     * Show the form for editing the specified Vacante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vacante = DB::table('vacantes')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->select('vacantes.*', 'proyectos.nombre as est', 'puestos.puesto as pst')
            ->where('vacantes.id', '=', $id)
            ->get();
        $proyectos = Proyecto::all();
        $puestos = Puesto::all();
        if (empty($vacante)) {
            Flash::error('Vacante not found');

            return redirect(route('vacantes.index'));
        }
        //dd($vacante[0]);
        return view('vacantes.edit')
            ->with('vacante', $vacante[0])
            ->with('proyectos', $proyectos)
            ->with('puestos', $puestos);
    }

    /**
     * Update the specified Vacante in storage.
     *
     * @param int $id
     * @param UpdateVacanteRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $input = request()->validate([
            'user' => ['required'],
            'estacionamiento' => [''],
            'puesto' => [''],
            "horario" => ['required'],
            "fecha_submit" => ['required'],
            "salario" => ['required'],
            "tel" => ['required'],
            "email" => ['required'],
            "estado" => ['required']
        ]);
        $vacante = $this->vacanteRepository->find($id);
        if (empty($vacante)) {
            Flash::error('Vacante not found');

            return redirect(route('vacantes.index'));
        }
        $vacante->fecha = $input['fecha_submit'];
        $vacante->salario = $input['salario'];
        $vacante->tel = $input['tel'];
        $vacante->email = $input['email'];
        $vacante->estado = $input['estado'];
        $vacante->horario = $input['horario'];
        if ($input['estacionamiento'] != null) {
            $vacante->id_proyecto = $input['estacionamiento'];
        }
        if ($input['puesto'] != null) {
            $vacante->id_puesto = $input['puesto'];
        }
        $vacante->save();
        //$vacante = $this->vacanteRepository->update($request->all(), $id);

        Flash::success('Vacante actualizada.');

        return redirect(route('vacantes.index'));
    }

    /**
     * Remove the specified Vacante from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vacante = $this->vacanteRepository->find($id);

        if (empty($vacante)) {
            Flash::error('Vacante not found');

            return redirect(route('vacantes.index'));
        }

        $this->vacanteRepository->delete($id);

        Flash::success('Vacante deleted successfully.');

        return redirect(route('vacantes.index'));
    }

    public function postulados($id)
    {
        $postulados = DB::table('postulantes')
            ->where('id_vacante', '=', $id)
            ->get();
        return view('vacantes.postulados')->with('postulados', $postulados);
    }

    public function actualizaPostulado($id)
    {
        $input = \request()->all();
        $postulante = Postulante::find($id);
        $postulante->estado = $input['btn_accion'];
        $postulante->save();
        Flash::success('Postulante Actualizado');
        return redirect(route('vacantes.postulados', $postulante->id_vacante));
    }
}
