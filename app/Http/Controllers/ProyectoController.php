<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProyectoRequest;
use App\Http\Requests\UpdateProyectoRequest;
use App\Models\Gerente;
use App\Models\Marca;
use App\Models\Proyecto;
use App\Repositories\ProyectoRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class ProyectoController extends AppBaseController
{
    /** @var  ProyectoRepository */
    private $proyectoRepository;

    public function __construct(ProyectoRepository $proyectoRepo)
    {
        $this->proyectoRepository = $proyectoRepo;
    }

    /**
     * Display a listing of the Proyecto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $proyectos = DB::table('proyectos')
            ->join('gerentes', 'gerentes.id', '=', 'proyectos.id_gerente')
            ->select('proyectos.id', 'proyectos.nombre', 'proyectos.direccion', 'gerentes.nombre as gerente', 'proyectos.escuela')
            ->get();
        return view('proyectos.index')
            ->with('proyectos', $proyectos);
    }

    /**
     * Show the form for creating a new Proyecto.
     *
     * @return Response
     */
    public function create()
    {
        $gerentes = Gerente::all();
        $marcas = Marca::all();
        return view('proyectos.create')
            ->with('gerentes', $gerentes)
            ->with('marcas', $marcas);
    }

    /**
     * Store a newly created Proyecto in storage.
     *
     * @param CreateProyectoRequest $request
     *
     * @return Response
     */
    public function store()
    {
        /*$input = request()->all();
        dd($input);*/
        $input = request()->validate([
            "nombre" => ['required'],
            "pais" => ['required',],
            "edo_prov" => ['required'],
            "mun_cd" => ['required'],
            "colonia" => ['required'],
            "calles" => ['required'],
            "numeroExt" => ['required'],
            "cp" => ['required'],
            "num_vac" => ['required'],
            "gerente" => ['required'],
            "marca" => ['required'],
            "direccion" => ['required'],
            "latitud" => ['required'],
            "longitud" => ['required'],
            "escuela" => ['required'],
            'no_est' => ['required']
        ]);
        $foto = request()->file('file');
        $dataImg = $foto->get();
        $img = base64_encode($dataImg);
        $proyecto = new Proyecto();
        if ($input['escuela'] == 'true') {
            $proyecto->fill([
                'id_gerente' => $input['gerente'],
                'id_marca' => $input['marca'],
                'nombre' => $input['nombre'],
                'direccion' => $input['direccion'],
                'pais' => strtoupper($input['pais']),
                'edo_prov' => strtoupper($input['edo_prov']),
                'mun_cd' => strtoupper($input['mun_cd']),
                'colonia' => strtoupper($input['colonia']),
                'calles' => strtoupper($input['calles']),
                'numeroExt' => strtoupper($input['numeroExt']),
                'cp' => $input['cp'],
                'latitud' => $input['latitud'],
                'longitud' => $input['longitud'],
                'status' => 1,
                'num_vac' => $input['num_vac'],
                'foto' => $img,
                'escuela' => true,
                'no_est' => $input['no_est']
            ]);
        } else {
            $proyecto->fill([
                'id_gerente' => $input['gerente'],
                'id_marca' => $input['marca'],
                'nombre' => $input['nombre'],
                'direccion' => $input['direccion'],
                'pais' => strtoupper($input['pais']),
                'edo_prov' => strtoupper($input['edo_prov']),
                'mun_cd' => strtoupper($input['mun_cd']),
                'colonia' => strtoupper($input['colonia']),
                'calles' => strtoupper($input['calles']),
                'numeroExt' => strtoupper($input['numeroExt']),
                'cp' => $input['cp'],
                'latitud' => $input['latitud'],
                'longitud' => $input['longitud'],
                'status' => 1,
                'num_vac' => $input['num_vac'],
                'foto' => $img,
                'escuela' => false,
                'no_est' => $input['no_est']
            ]);
        }
        $proyecto->save();
        /*$proyecto = $this->proyectoRepository->create($input);*/

        Flash::success('Proyecto añadido satisfactoriamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Display the specified Proyecto.
     *
     * @param int $id
     *
     * @return Response
     */


    public function show($id)
    {
        $proyecto = $this->proyectoRepository->find($id);

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        return view('proyectos.show')->with('proyecto', $proyecto);
    }

    /**
     * Show the form for editing the specified Proyecto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proyecto = DB::table('proyectos')
            ->join('gerentes', 'proyectos.id_gerente', '=', 'gerentes.id')
            ->join('marcas', 'proyectos.id_marca', '=', 'marcas.id')
            ->select('proyectos.*', 'gerentes.nombre as gerente', 'marcas.marca')
            ->where('proyectos.id', '=', $id)
            ->get();
        $gerentes = Gerente::all();
        $marcas = Marca::all();
        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }
        return view('proyectos.edit')
            ->with('proyecto', $proyecto[0])
            ->with('gerentes', $gerentes)
            ->with('marcas', $marcas);
    }

    /**
     * Update the specified Proyecto in storage.
     *
     * @param int $id
     * @param UpdateProyectoRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $input = request()->validate([
            "nombre" => ['required'],
            "pais" => ['required',],
            "edo_prov" => ['required'],
            "mun_cd" => ['required'],
            "colonia" => ['required'],
            "calles" => ['required'],
            "numeroExt" => ['required'],
            "cp" => ['required'],
            "gerente" => [''],
            "marca" => [''],
            "direccion" => ['required'],
            "latitud" => ['required'],
            "longitud" => ['required'],
            "escuela" => [''],
            'no_est' => ['required']
        ]);
        $proyecto = $this->proyectoRepository->find($id);
        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }
        $proyecto->nombre = $input['nombre'];
        $proyecto->direccion = $input['direccion'];
        $proyecto->pais = $input['pais'];
        $proyecto->edo_prov = $input['edo_prov'];
        $proyecto->mun_cd = $input['mun_cd'];
        $proyecto->colonia = $input['colonia'];
        $proyecto->calles = $input['calles'];
        $proyecto->numeroExt = $input['numeroExt'];
        $proyecto->cp = $input['cp'];
        $proyecto->latitud = $input['latitud'];
        $proyecto->longitud = $input['longitud'];
        $proyecto->no_est = $input['no_est'];
        if (is_numeric($input['gerente'])) {
            $proyecto->id_gerente = $input['gerente'];
        }
        if (is_numeric($input['marca'])) {
            $proyecto->id_marca = $input['marca'];
        }
        if ($input['escuela'] == 'true') {
            $proyecto->escuela = true;
        } elseif ($input['escuela'] == 'false') {
            $proyecto->escuela = false;
        }
        $foto = request()->file('file');
        if ($foto != null) {
            $dataImg = file_get_contents($foto->getLinkTarget());
            $img = base64_encode($dataImg);
            $proyecto->foto = $img;
        }
        $proyecto->save();

        Flash::success('Proyecto actualizado satisfactoriamente.');

        return redirect(route('proyectos.index'));
    }

    /**
     * Remove the specified Proyecto from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $proyecto = $this->proyectoRepository->find($id);

        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');

            return redirect(route('proyectos.index'));
        }

        $this->proyectoRepository->delete($id);

        Flash::success('Proyecto eliminado satisfactoriamente.');

        return redirect(route('proyectos.index'));
    }
}
