<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostulanteRequest;
use App\Http\Requests\UpdatePostulanteRequest;
use App\Repositories\PostulanteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Response;

class PostulanteController extends AppBaseController
{
    /** @var  PostulanteRepository */
    private $postulanteRepository;

    public function __construct(PostulanteRepository $postulanteRepo)
    {
        $this->postulanteRepository = $postulanteRepo;
    }

    /**
     * Display a listing of the Postulante.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $postulantes = $this->postulanteRepository->all();

        return view('postulantes.index')
            ->with('postulantes', $postulantes);
    }

    /**
     * Show the form for creating a new Postulante.
     *
     * @return Response
     */
    public function create($id)
    {
        return view('postulantes.create')
            ->with('unico', $id);
    }

    /**
     * Store a newly created Postulante in storage.
     *
     * @param CreatePostulanteRequest $request
     *
     * @return Response
     */
    public function store(CreatePostulanteRequest $request)
    {
        $input = $request->all();
        $post = DB::table('postulantes')
            ->join('vacantes','vacantes.id','=','postulantes.id_vacante')
            ->where('postulantes.correo','=',$input['correo'])
            ->where('vacantes.id','=',$input['id_vacante'])
            ->select('postulantes.correo')
            ->distinct()
            ->get();
        if (empty($post[0])) {
            $postulante = $this->postulanteRepository->create($input);
            Flash::success('Tu postulación fue recibida con éxito.');
        }else{
            Flash::error('Este correo ya esta postulado a esta vacante');
        }
        return redirect('/');
    }

    /**
     * Display the specified Postulante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            Flash::error('Postulante not found');

            return redirect(route('postulantes.index'));
        }

        return view('postulantes.show')->with('postulante', $postulante);
    }

    /**
     * Show the form for editing the specified Postulante.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            Flash::error('Postulante not found');

            return redirect(route('postulantes.index'));
        }

        return view('postulantes.edit')->with('postulante', $postulante);
    }

    /**
     * Update the specified Postulante in storage.
     *
     * @param int $id
     * @param UpdatePostulanteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostulanteRequest $request)
    {
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            Flash::error('Postulante not found');

            return redirect(route('postulantes.index'));
        }

        $postulante = $this->postulanteRepository->update($request->all(), $id);

        Flash::success('Postulante updated successfully.');

        return redirect(route('postulantes.index'));
    }

    /**
     * Remove the specified Postulante from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $postulante = $this->postulanteRepository->find($id);

        if (empty($postulante)) {
            Flash::error('Postulante not found');

            return redirect(route('postulantes.index'));
        }

        $this->postulanteRepository->delete($id);

        Flash::success('Postulante deleted successfully.');

        return redirect(route('postulantes.index'));
    }
}
