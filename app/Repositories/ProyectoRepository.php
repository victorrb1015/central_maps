<?php

namespace App\Repositories;

use App\Models\Proyecto;
use App\Repositories\BaseRepository;

/**
 * Class ProyectoRepository
 * @package App\Repositories
 * @version September 2, 2019, 4:12 pm UTC
 */

class ProyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_gerente',
        'id_marca',
        'nombre',
        'direccion',
        'pais',
        'edo_prov',
        'mun_cd',
        'colonia',
        'calles',
        'numeroExt',
        'cp',
        'latitud',
        'longitud',
        'status',
        'num_vac',
        'foto',
        'escuela',
        'no_est'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Proyecto::class;
    }
}
