@extends('layouts.app')

@section('content')
    <style>
        .scrollbar-deep-purple::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #F5F5F5;
            border-radius: 10px; }

        .scrollbar-deep-purple::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5; }

        .scrollbar-deep-purple::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #512da8; }

        .scrollbar-cyan::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #F5F5F5;
            border-radius: 10px; }

        .scrollbar-cyan::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5; }

        .scrollbar-cyan::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #00bcd4; }

        .scrollbar-dusty-grass::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #F5F5F5;
            border-radius: 10px; }

        .scrollbar-dusty-grass::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5; }

        .scrollbar-dusty-grass::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-image: -webkit-linear-gradient(330deg, #d4fc79 0%, #96e6a1 100%);
            background-image: linear-gradient(120deg, #d4fc79 0%, #96e6a1 100%); }

        .scrollbar-ripe-malinka::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-color: #F5F5F5;
            border-radius: 10px; }

        .scrollbar-ripe-malinka::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5; }

        .scrollbar-ripe-malinka::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
            background-image: -webkit-linear-gradient(330deg, #f093fb 0%, #f5576c 100%);
            background-image: linear-gradient(120deg, #f093fb 0%, #f5576c 100%); }

        .bordered-deep-purple::-webkit-scrollbar-track {
            -webkit-box-shadow: none;
            border: 1px solid #512da8; }

        .bordered-deep-purple::-webkit-scrollbar-thumb {
            -webkit-box-shadow: none; }

        .bordered-cyan::-webkit-scrollbar-track {
            -webkit-box-shadow: none;
            border: 1px solid #00bcd4; }

        .bordered-cyan::-webkit-scrollbar-thumb {
            -webkit-box-shadow: none; }

        .square::-webkit-scrollbar-track {
            border-radius: 0 !important; }

        .square::-webkit-scrollbar-thumb {
            border-radius: 0 !important; }

        .thin::-webkit-scrollbar {
            width: 6px; }

        .tamano {
            position: relative;
            overflow-y: scroll;
            height: 510px; }
    </style>

    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Detalles de vacante</h3>

        </div>
    </div>
    <hr>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered results responsive-table text-center">
                    <thead>
                    <tr>
                        <th scope="col"><strong>Nombre de la vacante:</strong></th>
                        <th scope="col"><strong>Empresa/Proyecto:</strong></th>
                        <th scope="col"><strong>Área:</strong></th>
                        <th scope="col"><strong>Salario mensual:</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @forelse($proyectos as $item)
                            <td>{{$item->puesto}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->area}}</td>
                            <td>{{$item->salario}}</td>
                        @empty
                            <td>Sin puestos</td>
                        @endforelse
                    </tr>
                    <tr>
                        <th scope="col"><strong>Fecha:</strong></th>
                        <th scope="col"><strong>País:</strong></th>
                        <th scope="col"><strong>Estado/Provincia:</strong></th>
                        <th scope="col"><strong>Municipio/Ciudad:</strong></th>
                    </tr>
                    <tr>
                        @forelse($proyectos as $item)
                            <td>{{$item->fecha}}</td>
                            <td>{{$item->pais}}</td>
                            <td>{{$item->edo_prov}}</td>
                            <td>{{$item->mun_cd}}</td>
                        @empty
                            <td>Sin puestos</td>
                        @endforelse
                    </tr>
                    <tr>
                        <th scope="col"><strong>Teléfono:</strong></th>
                        <th scope="col"><strong>Email:</strong></th>
                        <th scope="col"><strong>Descripcion:</strong></th>
                        <th scope="col"><strong>Requisitos:</strong></th>
                    </tr>
                    <tr>
                        @forelse($proyectos as $item)
                            <td>{{$item->tel}}</td>
                            <td>{{$item->email}}</td>
                            <td>{!! wordwrap($item->des,30,"<br />\n") !!}</td>
                            <td>{!! wordwrap($item->req,30,"<br />\n") !!}</td>
                        @empty
                            <td>Sin puestos</td>
                        @endforelse
                    </tr>
                    </tbody>
                </table>
                <a class="btn btn-default btn-rounded" id="calcula_ruta">Obtener ruta</a>
                <a class="btn btn-default btn-rounded float-lg-right" id="postularme"
                   href="{{route('post', $proyectos[0]->unico)}}">Postularme</a>
            </div>
        </div>

        <style>#buscar_ruta {
                display: none
            }
        </style>

        <br>
        <div class="card card-cascade wider" id="buscar_ruta">

            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">

                <!-- Title -->
                <h2 class="card-header-title mb-3">Detalles de ruta</h2>

            </div>

            <!-- Card content -->
            <div class="card-body card-body-cascade text-center">
                <!-- Text -->
                <div class="card-text">
                    <div class="form-row">
                        <!--<div class="col">
                             First name
                            <div class="md-form">
                                <input type="text" id="materialRegisterFormFirstName" class="form-control">
                                <label for="materialRegisterFormFirstName">Dirección partida</label>
                            </div>
                        </div>-->
                        <div class="col">
                            <!-- Last name -->
                            <div class="md-form">
                                <input type="text" value="{{$proyectos[0]->direccion}}" class="form-control">
                                <label for="materialRegisterFormLastName">Dirección estacionamiento</label>
                            </div>
                        </div>
                        <div class="col">
                            <select class="mdb-select md-form colorful-select dropdown-default" id="mode"
                                    onchange="calcRoute()">
                                <option value="DRIVING" selected>Coche</option>
                                <option value="WALKING">Caminando</option>
                                <option value="BICYCLING">Bicicleta</option>
                                <option value="TRANSIT">Transporte publico</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div id="map_canvas" class="z-depth-1-half map-container" style="height: 510px"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body card-body-cascade text-center tamano scrollbar-dusty-grass">
                                <div id="directionsPanel" style="width:100%; height:100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card content -->

        </div>
        <!-- Card -->

        <br>
        @endsection

        <script>
                    @push('scripts')
            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer();
            var ltdUsuario;
            var lgtUsuario;

            navigator.geolocation.getCurrentPosition(success, error);

            $(document).ready(function () {
                $('.mdb-select').materialSelect();
            });

            function success(position) {
                var coordenadas = position.coords;
                ltdUsuario = coordenadas.latitude;
                lgtUsuario = coordenadas.longitude;
            }

            function error(error) {
                console.warn('ERROR(' + error.code + '): ' + error.message);
            }

            $('#calcula_ruta').click(function () {
                $('#buscar_ruta').show();
                navigator.geolocation.getCurrentPosition(success, error);
                initMap();
            });

            function initMap() {
                var usuario = new google.maps.LatLng(ltdUsuario, lgtUsuario);
                var mapOptions = {
                    zoom: 14,
                    center: usuario
                }
                var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                directionsDisplay.setMap(map);
                directionsDisplay.setPanel(document.getElementById('directionsPanel'));
                calcRoute1();
            }

            function calcRoute1() {
                var usuario = new google.maps.LatLng(ltdUsuario, lgtUsuario);
                var proyecto = new google.maps.LatLng({{$proyectos[0]->latitud}}, {{$proyectos[0]->longitud}});
                var request = {
                    origin: usuario,
                    destination: proyecto,
                    travelMode: 'DRIVING'
                };
                directionsService.route(request, function (result, status) {
                    console.log(status);
                    if (status == 'OK') {
                        directionsDisplay.setDirections(result);
                        console.log(result)
                    }
                });
            }

            function calcRoute() {
                var selectedMode = document.getElementById('mode').value;
                var usuario = new google.maps.LatLng(ltdUsuario, lgtUsuario);
                var proyecto = new google.maps.LatLng({{$proyectos[0]->latitud}}, {{$proyectos[0]->longitud}});
                var request = {
                    origin: usuario,
                    destination: proyecto,
                    // Note that Javascript allows us to access the constant
                    // using square brackets and a string value as its
                    // "property."
                    travelMode: google.maps.TravelMode[selectedMode]
                };
                directionsService.route(request, function (response, status) {
                    if (status == 'OK') {
                        directionsDisplay.setDirections(response);
                    }
                });
            }
            @endpush
        </script>
