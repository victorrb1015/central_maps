@extends('layouts.app')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Búsqueda de vacantes</h3>

        </div>
    </div>

    <!--division entre formularios-->
    <hr>
    @include('layouts.errors')
    @include('flash::message')
    <!-- Card -->
    <div class="card">
        <!-- Card content -->
        <div class="card-body">
            <!-- Title -->
            <h4 class="card-title"><a>Busca la vacante más cercana</a></h4>
        @include('layouts.errors')
        <!-- Text -->
            <div class="card-text">
                <div class="row">
                    <div class="col-md-4">
                        <select class="mdb-select md-form colorful-select dropdown-default" searchable="Buscar..."
                                name="estacionamiento" id="pais" onchange="pais()">
                            <option value="" selected disabled>Seleccione un pais</option>
                            @forelse($pais as $item)
                                <option value="{{$item->pais}}">{{$item->pais}}</option>
                            @empty
                                <option value="">Sin paises</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="col-md-4 text-center">
                        <div id="estado"></div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div id="mun"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="resultado_personal"></div>



    <!--division entre formularios-->
    <hr>
    <!-- Card -->
    <div class="card">
        <!-- Card content -->
        <div class="card-body">
            <!-- Title -->
            <h4 class="card-title"><a>Busqueda por estacionamiento</a></h4>
        @include('layouts.errors')
        <!-- Text -->
            <div class="row">
                <div class="col-md-9 col-lg-10">
                    <select class="mdb-select md-form colorful-select dropdown-default" searchable="Buscar..."
                            name="estacionamiento" id="estacionamiento">
                        <option value="" selected disabled>Seleccione un estacionamiento</option>
                        @forelse($proyectos as $item)
                            <option value="{{$item->id}}">{{$item->nombre}}</option>
                        @empty
                            <option value="">Sin vacantes</option>
                        @endforelse
                    </select>
                </div>
                <div class="col-md-2">
                    {!! Form::submit('Buscar', ['class' => 'btn btn-primary', 'id' => 'buscar_estacionamiento', 'onclick' => "buscarEstacionamiento($('#estacionamiento').val());return false;"]) !!}
                </div>
            </div>
        </div>

    </div>

    <div id="resultado_estacionamiento"></div>

    <!--division entre formularios-->
    <hr>
    <!-- Card -->
    <div class="card">
        <!-- Card content -->
        <div class="card-body">
            <!-- Title -->
            <h4 class="card-title"><a>Busqueda por puesto</a></h4>
        @include('layouts.errors')
        <!-- Text -->
            <div class="row">
                <div class="col-md-9 col-lg-10">
                    <select class="mdb-select md-form colorful-select dropdown-default" searchable="Buscar..."
                            name="puesto" id="puesto">
                        <option value="" selected disabled>Seleccione un puesto</option>
                        @forelse($puestos as $item)
                            <option value="{{$item->id}}">{{$item->puesto}}</option>
                        @empty
                            <option value="">Sin puestos</option>
                        @endforelse
                    </select>
                </div>
                <div class="col-md-2">
                    {!! Form::submit('Buscar', ['class' => 'btn btn-primary', 'id' => 'buscar_puesto', 'onclick' => "buscaPuesto($('#puesto').val());return false;"]) !!}
                </div>
            </div>
        </div>
    </div>

    <div id="resultado"></div>


    <!--Termina card de puesto
    <hr>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><a>Busqueda por Marcas de estacionamiento</a></h4>
        @include('layouts.errors')
        <p class="card-text">
            <select class="mdb-select md-form colorful-select dropdown-default" searchable="Buscar..."
                    name="estacionamiento" id="marca">
                <option value="" selected disabled>Seleccione una Marca</option>
@forelse($marcas as $item)
        <option value="{{$item->id}}">{{$item->marca}}</option>
                    @empty
        <option value="">Sin Marcas</option>
@endforelse
        </select>
    </p>
    <div class="form-group col-sm-12">
{!! Form::submit('Buscar', ['class' => 'btn btn-primary', 'id' => 'buscar_marca', 'onclick' => "buscarMarca($('#marca').val());return false;"]) !!}
        </div>
    </div>
</div>
<div id="resultado_marca"></div>
-->
    <br>



@endsection

<script>
    @push('scripts')
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
    @endpush

    @push('scripts')
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function buscaPuesto(valorPuesto) {
        $.ajax({
            data: {valor: valorPuesto},
            url: '{{route('Puestos')}}',
            type: 'POST',
            beforeSend: function () {
                $("#resultado").html("<div class='progress md-progress primary-color-dark'>\n " +
                    "<div class='indeterminate'></div>\n</div>");
            },
            success: function (data) {
                console.log(data);
                $("#resultado").html(data);
                /*
                var dhtml = "";
                var direccion = ""
                for (datas in data.datos) {
                    dhtml += data.datos[datas].nombre;
                    direccion += data.datos[datas].direccion;

                    $("#resultado").html(
                        "<hr><h3>Vacantes encontradas</h3>\n"+
                        "<div class=\"card card-body\">\n" +
                        "      <h4 class=\"card-title\">Estacionamiento: "+ dhtml +"</h4>\n" +
                        "      <p class=\"card-text\"> Dirección: " + direccion + ".</p>\n" +
                        "      <div class=\"flex-row\">\n" +
                        "        <a class=\"card-link btn btn-danger\">Ver rutas y detalles</a>\n" +
                        "        <a class=\"card-link btn btn-primary\">Postularme</a>\n" +
                        "</div>"
                    );

                }*/

            },
            error: function () {
                $("#resultado").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }

    function buscarEstacionamiento(valorPuesto) {
        $.ajax({
            data: {valor: valorPuesto},
            url: '{{route('Estacionamiento')}}',
            type: 'POST',
            beforeSend: function () {
                $("#resultado_estacionamiento").html("<div class='progress md-progress primary-color-dark'>\n " +
                    "<div class='indeterminate'></div>\n</div>");
            },
            success: function (data) {
                console.log(data);
                $("#resultado_estacionamiento").html(data);
            },
            error: function () {
                $("#resultado_estacionamiento").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }

    function buscarMarca(valorPuesto) {
        $.ajax({
            data: {valor: valorPuesto},
            url: '{{route('marca')}}',
            type: 'POST',
            beforeSend: function () {
                $("#resultado_marca").html("<div class='progress md-progress primary-color-dark'>\n " +
                    "<div class='indeterminate'></div>\n</div>");
            },
            success: function (data) {
                console.log(data);
                $("#resultado_marca").html(data);
            },
            error: function () {
                $("#resultado_marca").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }

    function pais() {
        var pais = $('#pais').val();
        $.ajax({
            data: {pais: pais},
            url: '{{route('Pais')}}',
            type: 'POST',
            beforeSend: function () {
                $("#estado").html("<div class=\"preloader-wrapper big active\">\n" +
                    "  <div class=\"spinner-layer spinner-blue\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-red\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-yellow\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-green\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "</div>");
            },
            success: function (data) {
                console.log(data);
                $("#estado").html(data);
                $('.mdb-select2').materialSelect();
            },
            error: function () {
                $("#estado").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }

    function estado() {
        var pais = $('#pais').val();
        var estado = $('#estado_busca').val();
        $.ajax({
            data: {pais: pais, estado: estado},
            url: '{{route('Estado')}}',
            type: 'POST',
            beforeSend: function () {
                $("#mun").html("<div class=\"preloader-wrapper big active\">\n" +
                    "  <div class=\"spinner-layer spinner-blue\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-red\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-yellow\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "  <div class=\"spinner-layer spinner-green\">\n" +
                    "    <div class=\"circle-clipper left\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"gap-patch\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "    <div class=\"circle-clipper right\">\n" +
                    "      <div class=\"circle\"></div>\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "</div>");
            },
            success: function (data) {
                $("#mun").html(data);
                $('.mdb-select3').materialSelect();
            },
            error: function () {
                $("#mun").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }

    function mun() {
        var pais = $('#pais').val();
        var estado = $('#estado_busca').val();
        var mini = $('#munic').val();
        $.ajax({
            data: {pais: pais, estado: estado, municipio: mini},
            url: '{{route('Municipio')}}',
            type: 'POST',
            beforeSend: function () {
                $("#resultado_personal").html("<div class='progress md-progress primary-color-dark'>\n " +
                    "<div class='indeterminate'></div>\n</div>");
            },
            success: function (data) {
                $("#resultado_personal").html(data);
            },
            error: function () {
                $("#resultado_personal").html("<div class=\"alert alert-danger\" role=\"alert\">\n" +
                    " ¡Intente de nuevo!\n" + "</div>");
            }
        });
    }
    @endpush
</script>
