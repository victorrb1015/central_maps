@extends('layouts.app')

@section('title',  'Puestos')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Puestos</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')


    <div class="row">
        <div class="col-8">
            <button type="button" class="btn btn-default btn-lg">
                Puestos encontrados: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-4 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('puestos.create') !!}">
                Agregar puesto
            </a>
        </div>

    </div>
    <br>
    <!-- Empieza tabla -->
    @include('puestos.table')
    <!--Termina tabla-->


    <br><br>
@endsection