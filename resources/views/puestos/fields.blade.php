<!-- Puesto Field -->
<div class="md-form">
    <i class="fas fa-user-tie prefix"></i>
    {!! Form::label('puesto', 'Puesto:') !!}
    {!! Form::text('puesto', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="md-form">
    <i class="fas fa-chart-area prefix"></i>
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Req Field -->
<div class="form-group green-border-focus">
    <i class="fas fa-bullhorn prefix"></i>
    {!! Form::label('req', 'Requisitos:') !!}
    {!! Form::textarea('req', null, ['class' => 'md-textarea form-control']) !!}
</div>

<!-- Des Field -->
<div class="form-group green-border-focus">
    <i class="fab fa-slideshare prefix"></i>
    {!! Form::label('des', 'Descripción:') !!}
    {!! Form::textarea('des', null, ['class' => 'md-textarea form-control']) !!}
</div>

<!-- Submit Field -->
<div class="md-form">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('puestos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
