@extends('layouts.app')
@section('title',  'Puestos')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Crear puesto</h3>
        </div>
    </div>
    <hr>

            @include('layouts.errors')

            {!! Form::open(['route' => 'puestos.store']) !!}
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    @include('puestos.fields')
                </div>
            </div>

            {!! Form::close() !!}

    <br>
@endsection
