<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $puesto->id !!}</p>
</div>

<!-- Puesto Field -->
<div class="form-group">
    {!! Form::label('puesto', 'Puesto:') !!}
    <p>{!! $puesto->puesto !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $puesto->area !!}</p>
</div>

<!-- Req Field -->
<div class="form-group">
    {!! Form::label('req', 'Req:') !!}
    <p>{!! $puesto->req !!}</p>
</div>

<!-- Des Field -->
<div class="form-group">
    {!! Form::label('des', 'Des:') !!}
    <p>{!! $puesto->des !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $puesto->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $puesto->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $puesto->updated_at !!}</p>
</div>

