@extends('layouts.app')
@section('title',  'Puestos')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Editar puesto</h3>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-4">

        </div>
        <div class="col-4">
            @include('layouts.errors')

            {!! Form::model($puesto, ['route' => ['puestos.update', $puesto->id], 'method' => 'patch']) !!}
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    @include('puestos.fields')
                </div>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-4">

        </div>
    </div>
@endsection
