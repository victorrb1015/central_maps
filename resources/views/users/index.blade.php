@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="card card-cascade wilder">
            <!-- Card image -->
            <div class="view view-cascade gradient-card-header default-color">
                <!-- Title -->
                <h3 class="card-header-title">Usuarios</h3>
            </div>
        </div>
        <hr>
        @include('layouts.errors')
        @include('flash::message')
        <div class="row">
            <div class="col-6">
                <button type="button" class="btn btn-default btn-lg">
                    Usuarios encontrados: <span class="counter badge badge-danger ml-4"></span>
                </button>
            </div>
            <div class="col-3 text-right">
                <a type="button" class="btn btn-primary btn-rounded" href="{!! route('permisos.index') !!}">
                    Permisos
                </a>
            </div>
            <div class="col-3 text-right">
                <a type="button" class="btn btn-dark btn-rounded" href="{!! route('users.create') !!}">
                    Agregar Usuario
                </a>
            </div>
        </div>
        <br>
        <div class="form-group pull-right">
            <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
        </div>
        @include('users.table')
    </section>
@endsection

