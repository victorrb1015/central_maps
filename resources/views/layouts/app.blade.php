<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Desarrollador: Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <meta name="author" content="Desarrollador: Victor Rojas Barrera - victorrb1015@gmail.com">
    <title>@yield('title' , 'Vacantes')</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('logo/logo/login-logo.png')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
</head>
<style>
    body {
        padding: 20px 20px;
    }

    .results tr[visible='false'],
    .no-result {
        display: none;
    }

    .results tr[visible='true'] {
        display: table-row;
    }

    .counter {
        padding: 8px;
        color: white;
    }
</style>

@guest
    @include('layouts.nav')
@else
    @include('layouts.nav_central')
@endguest


<body style="padding-top: 90px;">
<main class="py-4">
    <div class="container">

        @yield('content')

    </div>
</main>
</body>
<footer class="page-footer font-small aqua-gradient  fixed-bottom">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?> Copyright:
        <a href="https:central-mx.com" target="_blank"> Operadora Central de Estacionamientos SAPI de C.V.</a>
    </div>
    <!-- Copyright -->

</footer>

<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript">
    //script para mensajes falsh
    $(window).load(function () {
        $(".loader").fadeOut("slow");
    });
</script>
<script>
    $('div.alert').not('.alert-important').delay(4500).fadeOut(300);
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key={{env('MAPS_KEY','AIzaSyBoN6cE0y9vrkzluAmHhRZVZXTACDqT2CI')}}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<!--Tabla con busqueda-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
            $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
                    return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });
            $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','false');
            });
            $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
                $(this).attr('visible','true');
            });

            var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');

            if(jobCount == '0') {$('.no-result').show();}
            else {$('.no-result').hide();}
        });
    });
</script>
<script>
    @stack('scripts')
</script>
</body>
</html>
