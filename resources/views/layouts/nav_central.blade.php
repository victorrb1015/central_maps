<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
    <div class="container smooth-scroll">
        <a class="navbar-brand " href="{{url('/')}}"><img src="{{asset('logo/logo/login-logo.png')}}"
                                                          style="width: 40px;">entral maps</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <div class="dropdown nav-link">
                        <a class="btn btn-default btn-rounded btn-sm dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false"><i class="fas fa-chevron-circle-down"></i>Menu</a>
                        <div class="dropdown-menu dropdown-default">
                            <a class="dropdown-item" href="{{url('/vacantes')}}">Vacantes</a>
                            <a class="dropdown-item" href="{{url('/gerentes')}}">Gerentes</a>
                            <a class="dropdown-item" href="{{url('/users')}}">Usuarios</a>
                            <a class="dropdown-item" href="{{url('/puestos')}}">Puestos</a>
                            <a class="dropdown-item" href="{{url('/proyectos')}}">Proyectos</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><span class="btn btn-default btn-rounded btn-sm">
                    <i class="fas fa-user" aria-hidden="true"></i>{{ Auth::user()->name }}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" type="#logout-form" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <span class="btn btn-default btn-rounded btn-sm">
                    <i class="fas fa-power-off" aria-hidden="true"></i>Cerrar sesión</span></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
        </div>
    </div>
</nav>
