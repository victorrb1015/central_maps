@extends('layouts.app')
@section('title', 'Gerentes')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Crear gerente</h3>
        </div>
    </div>
    <hr>

    <div class="col-12">
        @include('layouts.errors')

        {!! Form::open(['route' => 'gerentes.store']) !!}
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                @include('gerentes.fields')
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="col-4">

    </div>
    </div>
@endsection
