
<!-- Nombre Field -->
<div class="md-form">
    <i class="far fa-address-card prefix"></i>
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="md-form">
    <i class="far fa-envelope prefix"></i>
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Field -->
<div class="md-form">
    <i class="fas fa-phone prefix"></i>
    {!! Form::label('telefono', 'Telefono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Empresa Field -->
<div class="md-form">
    <i class="fas fa-building prefix"></i>
    {!! Form::label('empresa', 'Empresa:') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="md-form">
    <i class="fas fa-city prefix"></i>
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('gerentes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
