@extends('layouts.app')

@section('title', 'Gerentes')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Gerentes</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')


    <div class="row">
        <div class="col-8">
            <button type="button" class="btn btn-default btn-lg">
                Gerentes encontrados: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-4 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('gerentes.create') !!}">
                Agregar gerente
            </a>
        </div>
    </div>

    <br>

    @include('gerentes.table')

    <br><br>
    </div>
@endsection
