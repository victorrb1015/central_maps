<!-- Marca Field -->
<div class="md-form">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="md-form">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('marcas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
