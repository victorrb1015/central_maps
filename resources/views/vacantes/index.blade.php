@extends('layouts.app')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Vacantes</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')
    @include('layouts.errors')
    <div class="row">
        <div class="col-8">
            <button type="button" class="btn btn-default btn-lg">
                Vacantes encontradas: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-4 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('vacantes.create') !!}">
                Agregar Vacante
            </a>
        </div>
    </div>
    <br>
    <!-- Empieza tabla -->
    @include('vacantes.table')
    <!--Termina tabla-->


    <br><br>
@endsection

