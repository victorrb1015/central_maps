@extends('layouts.app')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Postulados</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')
    @include('layouts.errors')
    <div class="row">
        <div class="col text-left">
            <button type="button" class="btn btn-default btn-lg">
                Postulados encontrados: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
    </div>
    <br>

    <div class="table-responsive">
        <div class="form-group pull-right">
            <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
        </div>
        <table class="table table-hover table-bordered results responsive-table text-center">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Direccion</th>
                <th>Estado</th>
                <th>Acción</th>

            </tr>
            <tr class="warning no-result">
                <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                    registro con la información ingresada
                </td>
            </tr>
            </thead>

            <tbody>
            @foreach($postulados as $item)
                <tr>
                    <td>{{$item->nombre}}</td>
                    <td>{{$item->correo}}</td>
                    <td>{{$item->tel}}</td>
                    <td>{{$item->direccion}}</td>
                    <td>
                        @switch($item->estado)
                            @case('contratado')
                            <i class="fas fa-thumbs-up fa-2x material-tooltip-main" data-toggle="tooltip"
                               data-placement="bottom" title="contratado"></i>
                            @break
                            @case('postulado')
                            <i class="fas fa-exchange-alt fa-2x material-tooltip-main" data-toggle="tooltip"
                               data-placement="bottom" title="postulado"></i>
                            @break
                            @case('rechazado')
                            <i class="fas fa-times fa-2x material-tooltip-main" data-toggle="tooltip"
                               data-placement="bottom" title="rechazado"></i>
                            @break
                        @endswitch
                    </td>
                    <td>
                        {!! Form::open(['route' => ['Actualiza', $item->id], 'method' => 'POST']) !!}

                        {!! Form::button('Aceptar',['type' => 'submit', 'class' => 'btn btn-success btn-xs', 'onclick' => "return confirm('¿Estas seguro?')", 'name' => 'btn_accion', 'value' => 'contratado']) !!}
                        {!! Form::button('Rechazar' ,['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')" , 'name' => 'btn_accion', 'value' => 'rechazado']) !!}
                    </td>
                    {!! Form::close() !!}
                </tr>
            @endforeach
            </tbody>
        </table>


        <br><br>
        @endsection

        <script>
            @push('script')
            $(document).ready(function () {
                $('.mdb-select').materialSelect();
            });
            @endpush
        </script>
