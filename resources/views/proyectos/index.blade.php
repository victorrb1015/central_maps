@extends('layouts.app')
@section('title','Proyectos')
@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Estacionamientos</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')


    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-default btn-lg">
                Estacionamientos encontrados: <span class="counter badge badge-danger ml-4"></span>
            </button>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-primary btn-rounded" href="{!! route('marcas.index') !!}">
                Marcas
            </a>
        </div>
        <div class="col-3 text-right">
            <a type="button" class="btn btn-dark btn-rounded" href="{!! route('proyectos.create') !!}">
                Agregar Estacionamiento
            </a>
        </div>

    </div>
    <br>
    <!-- Empieza tabla -->
    @include('proyectos.table')
    <!--Termina tabla-->


    <br><br>
@endsection

<script>
    @push('scripts')
    $(function () {
        $('.material-tooltip-main').tooltip({
            template: '<div class="tooltip md-tooltip-main"><div class="tooltip-arrow md-arrow"></div><div class="tooltip-inner md-inner-main"></div></div>'
        });
    })
    @endpush
</script>
