<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $proyecto->id !!}</p>
</div>

<!-- Id Gerente Field -->
<div class="form-group">
    {!! Form::label('id_gerente', 'Id Gerente:') !!}
    <p>{!! $proyecto->id_gerente !!}</p>
</div>

<!-- Id Marca Field -->
<div class="form-group">
    {!! Form::label('id_marca', 'Id Marca:') !!}
    <p>{!! $proyecto->id_marca !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $proyecto->nombre !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $proyecto->direccion !!}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{!! $proyecto->pais !!}</p>
</div>

<!-- Edo Prov Field -->
<div class="form-group">
    {!! Form::label('edo_prov', 'Edo Prov:') !!}
    <p>{!! $proyecto->edo_prov !!}</p>
</div>

<!-- Mun Cd Field -->
<div class="form-group">
    {!! Form::label('mun_cd', 'Mun Cd:') !!}
    <p>{!! $proyecto->mun_cd !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $proyecto->colonia !!}</p>
</div>

<!-- Calles Field -->
<div class="form-group">
    {!! Form::label('calles', 'Calles:') !!}
    <p>{!! $proyecto->calles !!}</p>
</div>

<!-- Numeroext Field -->
<div class="form-group">
    {!! Form::label('numeroExt', 'Numeroext:') !!}
    <p>{!! $proyecto->numeroExt !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $proyecto->cp !!}</p>
</div>

<!-- Latitud Field -->
<div class="form-group">
    {!! Form::label('latitud', 'Latitud:') !!}
    <p>{!! $proyecto->latitud !!}</p>
</div>

<!-- Longitud Field -->
<div class="form-group">
    {!! Form::label('longitud', 'Longitud:') !!}
    <p>{!! $proyecto->longitud !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $proyecto->status !!}</p>
</div>

<!-- Num Vac Field -->
<div class="form-group">
    {!! Form::label('num_vac', 'Num Vac:') !!}
    <p>{!! $proyecto->num_vac !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $proyecto->foto !!}</p>
</div>

<!-- Escuela Field -->
<div class="form-group">
    {!! Form::label('escuela', 'Escuela:') !!}
    <p>{!! $proyecto->escuela !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $proyecto->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $proyecto->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $proyecto->deleted_at !!}</p>
</div>

